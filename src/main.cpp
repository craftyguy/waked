/*
Waked
Copyright (C) 2021  Robin Westermann <waked@seath.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <sdbus-c++/sdbus-c++.h>
#include <list>
#include <string>
#include <iostream>
#include <fstream>

#include "alarm.h"

const char* inhibitorDestinationName = "org.freedesktop.login1";
const char* inhibitorObjectPath = "/org/freedesktop/login1";
const char* inhibitorInterfaceName = "org.freedesktop.login1.Manager";
std::unique_ptr inhibitorProxy = sdbus::createProxy(inhibitorDestinationName, inhibitorObjectPath);
std::list<Alarm> alarmList;
sdbus::UnixFd suspendDelayLockFd;


void writeToRTC(std::uint64_t data) {
    std::cout << "Writing to RTC: " << data << std::endl;
    std::ofstream rtc("/sys/class/rtc/rtc0/wakealarm");
    if (rtc.is_open()) {
        rtc << data << std::endl;
    } else {
        std::cout << "ERROR: Couldn't open RTC to write" << std::endl;
    }
}

std::uint64_t readFromRTC() {
    std::cout << "Reading from RTC: ... ";
    std::string line;
    std::ifstream rtc;
    rtc.open ("/sys/class/rtc/rtc0/wakealarm");
    rtc >> line;
    rtc.close();
    std::cout << line << "=";
    std::uint64_t data = 0;
    if (line.size()) {
        data = std::stoul(line);
    }
    std::cout << data << std::endl;
    return data;
}

void rescedule()
{
    std::cout << "Rescedule..." << std::endl;
    std::time_t now = std::time(nullptr);
    alarmList.remove_if([now](Alarm &a){return a.getTime() < now;});

    if (alarmList.size()) {
        uint64_t localReadFromRTC = readFromRTC();
        if ((localReadFromRTC > alarmList.front().getTime()) || (!localReadFromRTC)) {
            writeToRTC(0);
            writeToRTC(alarmList.front().getTime());
        }
    }
}

std::string registerAlarm(const std::string& id, const std::uint64_t alarmTime)
{
    std::cout << "register alarm: id=" << id << " time="<< alarmTime << std::endl;
    Alarm newAlarm(id,alarmTime);
    alarmList.push_back(std::move(newAlarm));
    alarmList.sort([](Alarm &a, Alarm &b) {
                       return a.getTime() < b.getTime();
                   });

    rescedule();

    return "ok";
}

std::string removeAlarm(const std::string& id)
{
    std::cout << "remove alarm: id=" << id << std::endl;
    if ((alarmList.size())
            && (alarmList.front().getId() == id)
            && (alarmList.front().getTime() == readFromRTC())) {
        writeToRTC(0);
    }

    alarmList.remove_if([id](Alarm &a){return id == a.getId();});

    rescedule();

    return "ok";
}

std::string updateAlarm(const std::string& id, const std::uint64_t alarmTime)
{
    std::cout << "update alarm: id=" << id << " time="<< alarmTime << std::endl;
    removeAlarm(id);
    registerAlarm(id, alarmTime);

    return "ok";
}

void handleSuspend(const bool active) {
    if (active) {
        std::cout << "Checking alarm for suspend ..." << std::endl;
        rescedule();

        // Delay wake-up if alarm is near
        std::time_t now = std::time(nullptr);
        if ((alarmList.size()) && (alarmList.front().getTime() < now + 10UL)) {
            std::cout << "Next alarm too close. Wake up in 10 Seconds ..." << std::endl;
            writeToRTC(now + 10UL);
        }
        suspendDelayLockFd.reset();
    } else {
        inhibitorProxy->callMethod("Inhibit").onInterface(inhibitorInterfaceName).withArguments("sleep", "Waked", "Manage system wake-ups", "delay").storeResultsTo(suspendDelayLockFd);
        std::cout << "Woke up from suspend ..." << std::endl;
        rescedule();
    }
}

int main(int argc, char *argv[])
{
    // Create D-Bus connection to the system bus and requests name on it.
    const char* serviceName = "de.seath.Waked";
    auto connection = sdbus::createSystemBusConnection(serviceName);

    const char* wakedObjectPath = "/de/seath/Waked/Alarm";
    auto wakedDbusObject = sdbus::createObject(*connection, wakedObjectPath);


    const char* wakedInterfaceName = "de.seath.Waked";
    wakedDbusObject->registerMethod("Add").onInterface(wakedInterfaceName).implementedAs(&registerAlarm);
    wakedDbusObject->registerMethod("Update").onInterface(wakedInterfaceName).implementedAs(&updateAlarm);
    wakedDbusObject->registerMethod("Remove").onInterface(wakedInterfaceName).implementedAs(&removeAlarm);
    wakedDbusObject->finishRegistration();

    inhibitorProxy->uponSignal("PrepareForSleep").onInterface(inhibitorInterfaceName).call(&handleSuspend);  //[](const std::string& str){ onConcatenated(str); });
    inhibitorProxy->finishRegistration();

    inhibitorProxy->callMethod("Inhibit").onInterface(inhibitorInterfaceName).withArguments("sleep", "Waked", "Manage system wake-ups", "delay").storeResultsTo(suspendDelayLockFd);

    // Run the loop on the connection.
    connection->enterEventLoop();
}
