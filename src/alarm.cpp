/*
Waked
Copyright (C) 2021  Robin Westermann <waked@seath.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "alarm.h"

std::uint64_t Alarm::getTime() const
{
    return time;
}

std::string Alarm::getId() const
{
    return id;
}

Alarm::Alarm(std::string id, uint64_t time) : id(id), time(time)
{

}
